

float random_float(float low, float high)
{
	float n=genrand_real1()*(high-low)+low;
	if (n<low) n=low;
	if (n>high) n=high;
	return n;
}

int random_int(int low, int high)
{
	int n=(int)(genrand_real1()*(1.0+high-low)+low-0.5);
	if (n<low) n=low;
	if (n>high) n=high;
	return n;
}

float rgb(float input)
{
	float output=input*255.0;
	if (output > 255.0) output=255.0;
	if (output < 0.0) output=0.0;
	return output;
}

int init_(void)
{
	for (int i=0; i<256; i++) {
		for (int j=0; j<256; j++) {
			ints[i][j]=0;
		}
	}
	char c;
	FILE *fp1;
	fp1 = fopen(load_level, "r");

	for (int i = 0; i < file_bytes; i++) infile[i] = 0;
	while (1) {
		c = fgetc(fp1);
		if (c == EOF)
			break;
		else

			infile[counter] = c;
		//printf("%c",c);

		counter++;
	}
	fclose(fp1);
	counter_column=0;
	counter_row=0;
	int char_counter=0;
	if(1) {
		for (int i = 0; i < counter; i++) {
			s[counter_column][counter_row][char_counter]=0;
			if ((infile[i] > 47 && infile[i] < 58)||infile[i] == '-') {
				s[counter_column][counter_row][char_counter]=infile[i];
				s[counter_column][counter_row][++char_counter]=0;
				//char_counter++;
			}
			if (infile[i] == ',') {
				counter_column++;
				char_counter=0;
			}
			if (infile[i] == 0x0a) {
				counter_row++;
				counter_column=0;
				char_counter=0;
			}
		}
	}
    if(1) {
        for (int i = 0; i<counter_row; i++) {
            for (int j=0; j<9; j++) {
                char ch[48];
                for (int k=0; k<48; k++)ch[k]=0;
                for (int k=0; s[j][i][k]!=0; k++) {
                    ch[k]=s[j][i][k];
                    ch[k+1]=0;
                }
                ints[j][i]=strtol(ch,NULL,10);
                //printf("%i,",ints[j][i]);
            }
            //printf(",,%i\n",i);
        }
    }
	//printf("%i\n",counter_row);
}

