//main.c
//sudo apt install -y liballegro5-dev build-essential astyle
//gcc main.c -o main -I/usr/include/allegro5 -L/usr/lib -lm -lallegro -lallegro_acodec -lallegro_audio -lallegro_color -lallegro_dialog -lallegro_font -lallegro_image -lallegro_memfile -lallegro_physfs -lallegro_primitives -lallegro_ttf
//astyle --indent=tab main.c init.h keyboard.h util.h

#include <math.h>
#include <errno.h>
#include <stdlib.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_color.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_memfile.h>
#include <allegro5/allegro_physfs.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_ttf.h>

#define load_level "chapter1.csv"
#define file_bytes 1234567

char infile[file_bytes];
char input_chars[18];
int s[256][256][48];
int ints[256][256];
int unrolled[256][65536];
int counter_row=0;
int counter_column=0;

int difference=0;

int counter=0;

const float FPS = 60;
int is_paused=1;
int display_width=0;
int display_height=0;
float mouse_cursor_x=0.0;
float mouse_cursor_y=0.0;

#include "keys.h"

bool doexit = false;

const int HOW_MANY_PARTICLES=100;
float particles[15][1000000];
int x=0;
int y=1;
int z=2;
int xr=9;
int yr=10;
int zr=11;
int xt=12;
int yt=13;
int zt=14;
int x_speed=3;
int y_speed=4;
int z_speed=5;
int r=6;
int g=7;
int b=8;
int bearing=3;
int pitch=4;
int yaw=5;
double camera[6]= {0,0,0,0,0,0};
int up=0;
int forward=1;
int right=2;
double unitvectors[3][3]= {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
int bruteforcethevectormatrices=0;

#include "mersenne_t.h"
#include "util_allegro.h"
#include "util.h"

int ev_key_up(int which_key) {
	if (which_key == ALLEGRO_KEY_ESCAPE) //doexit=true;
		return 0;
}

int ev_key_down(int which_key) {
	if (which_key == ALLEGRO_KEY_ESCAPE) is_paused=(is_paused+1)%2;
	if (which_key == ALLEGRO_KEY_C) {
		if (key[KEY_LCTRL]) doexit=true;
	}
	return 0;
}


int main(int argc, char **argv) {

	init_();

	for (int i = 1; i < HOW_MANY_PARTICLES; i++)
	{
		particles[x][i]=0.0;
		particles[y][i]=10.0;
		particles[z][i]=0.0;
		particles[x][i]=random_float(-100.0,100.0);
		particles[y][i]=random_float(-100.0,100.0);
		particles[z][i]=random_float(-100.0,100.0);
		particles[x_speed][i]=0.0;
		particles[y_speed][i]=0.0;
		particles[z_speed][i]=0.0;
		particles[r][i]=0.0;
		particles[g][i]=0.0;
		particles[b][i]=0.0;
		particles[r][i]=random_float(0.0,1.0);
		particles[g][i]=random_float(0.0,1.0);
		particles[b][i]=random_float(0.0,1.0);
	}

#include "init.h"
	int lesser_display_dimension=display_height;
	if (display_height>display_width) lesser_display_dimension=display_width;

	int which_answer=0;
	camera[bearing]=0.0;

	while(!doexit)
	{
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);

		if(ev.type == ALLEGRO_EVENT_TIMER) {
			if(0) {
				for (int i = 1; i < HOW_MANY_PARTICLES; i++)
				{
					particles[x][i]+=particles[x_speed][i];
					particles[y][i]+=particles[y_speed][i];
					particles[z][i]+=particles[z_speed][i];
					particles[z_speed][i]-=0.1;
				}
				for (int i = counter; i <= counter; i++)
				{
					particles[x][i]=0.0;
					particles[y][i]=10.0;
					particles[z][i]=0.0;
					float heading=random_float(0.0,2.0*3.141596);
					float magnitude=random_float(0.0,2.0*3.141596);
					particles[x_speed][i]=sin(heading)*magnitude;
					particles[y_speed][i]=cos(heading)*magnitude;
					int min=-10.0;
					int max=10.0;
					particles[z_speed][i]=random_float(1.0,20.0);
					particles[r][i]=random_float(0.0,1.0);
					particles[g][i]=random_float(0.0,1.0);
					particles[b][i]=random_float(0.0,1.0);
				}
			}

			//right is x+, up is z+, forward is y+
			unitvectors[forward][x]=sin(2.0*3.141596*camera[bearing]/360.0)*cos(2.0*3.141596*camera[pitch]/360.0);
			unitvectors[forward][y]=cos(2.0*3.141596*camera[bearing]/360.0)*cos(2.0*3.141596*camera[pitch]/360.0);
			unitvectors[forward][z]=sin(2.0*3.141596*camera[pitch]/360.0);
			unitvectors[up][x]=sin(2.0*3.141596*camera[bearing]/360.0)*( 0.0-sin(2.0*3.141596*camera[pitch]/360.0) );
			unitvectors[up][y]=cos(2.0*3.141596*camera[bearing]/360.0)*( 0.0-sin(2.0*3.141596*camera[pitch]/360.0) );
			unitvectors[up][z]=cos(2.0*3.141596*camera[pitch]/360.0);
			unitvectors[right][x]=cos(2.0*3.141596*camera[bearing]/360.0);
			unitvectors[right][y]=0.0-sin(2.0*3.141596*camera[bearing]/360.0);
			unitvectors[right][z]=0.0;
			
			float speed=1.1;
			int diagonal=-1;
			if (key[KEY_W]) {
				camera[x]+=unitvectors[forward][x]*speed;
				camera[y]+=unitvectors[forward][y]*speed;
				camera[z]+=unitvectors[forward][z]*speed;
				diagonal++;
			}
			if (key[KEY_S]) {
				camera[x]-=unitvectors[forward][x]*speed;
				camera[y]-=unitvectors[forward][y]*speed;
				camera[z]-=unitvectors[forward][z]*speed;
				diagonal++;
			}
			if (key[KEY_A]) {
				camera[x]-=unitvectors[right][x]*speed;
				camera[y]-=unitvectors[right][y]*speed;
				camera[z]-=unitvectors[right][z]*speed;
				diagonal++;
			}
			if (key[KEY_D]) {
				camera[x]+=unitvectors[right][x]*speed;
				camera[y]+=unitvectors[right][y]*speed;
				camera[z]+=unitvectors[right][z]*speed;
				diagonal++;
			}
			if (diagonal%2) speed*=pow(2.0,0.5);
			for (int i=0; i<HOW_MANY_PARTICLES; i++) {
				particles[xt][i]=particles[x][i]-camera[x];
				particles[yt][i]=particles[y][i]-camera[y];
				particles[zt][i]=particles[z][i]-camera[z];
				particles[xr][i]=
				    unitvectors[right][x]*particles[xt][i]+
				    unitvectors[right][y]*particles[yt][i]+
				    unitvectors[right][z]*particles[zt][i];
				particles[yr][i]=
				    unitvectors[forward][x]*particles[xt][i]+
				    unitvectors[forward][y]*particles[yt][i]+
				    unitvectors[forward][z]*particles[zt][i];
				particles[zr][i]=
				    unitvectors[up][x]*particles[xt][i]+
				    unitvectors[up][y]*particles[yt][i]+
				    unitvectors[up][z]*particles[zt][i];
			}
			redraw = true;
		}
		else if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
			doexit=true;
		}
		else if(ev.type == ALLEGRO_EVENT_MOUSE_AXES ||
		        ev.type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY) {

			mouse_x += ev.mouse.dx * (is_paused+1)%2;
			mouse_y += ev.mouse.dy * (is_paused+1)%2;
			
			camera[bearing]+=ev.mouse.dx * (is_paused+1)%2;
			if (camera[bearing]>360.0)camera[bearing]-=360.0;
			if (camera[bearing]< -0.0)camera[bearing]+=360.0;
			camera[pitch]+=ev.mouse.dy * (is_paused+1)%2;
			if (camera[pitch]>90.0)camera[pitch]=90.0;
			if (camera[pitch]< -90.0)camera[pitch]=-90.0;
			
			mouse_cursor_x += ev.mouse.dx*is_paused;
			mouse_cursor_y += ev.mouse.dy*is_paused;
			if (mouse_cursor_x < 0.0) mouse_cursor_x = 0.0;
			if (mouse_cursor_y < 0.0) mouse_cursor_y = 0.0;
			if (mouse_cursor_x > display_width) mouse_cursor_x = display_width;
			if (mouse_cursor_y > display_height) mouse_cursor_y = display_height;
			al_set_mouse_xy(display, display_width/2, display_height/2);
		}
		else if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) {
			al_set_sample_instance_playmode(sample, ALLEGRO_PLAYMODE_ONCE);
			al_play_sample_instance(sample);
			bruteforcethevectormatrices++;
			sample_time = al_get_sample_instance_time(sample);
			//fprintf(stderr, "Playing '%s' (%.3f seconds) 3 times", filename, sample_time);

			if (!al_set_sample_instance_gain(sample, 0.05)) {
				fprintf(stderr, "Failed to set gain.\n");
			}

			//al_rest(sample_time);

			if (mouse_cursor_x>display_width*.98&&mouse_cursor_x<display_width*.99
			        &&mouse_cursor_y>display_width*.01&&mouse_cursor_y<display_width*.02) doexit=1;
		}
		else if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {
			//doexit=true;
		}

#include "keyboard.h"

		if(redraw && al_is_event_queue_empty(event_queue)) {
			redraw = false;
			al_clear_to_color(al_map_rgb(0,0,0));
			if(1) {
				for (int i=0; i<HOW_MANY_PARTICLES; i++) {
					if (particles[yr][i]>0.1) {
						al_draw_filled_circle(
						    display_width/2+(200.0*particles[xr][i]/particles[yr][i]),
						    display_height/2-(200.0*particles[zr][i]/particles[yr][i]),
						    2.0, al_map_rgb(rgb(particles[r][i]),rgb(particles[g][i]),rgb(particles[b][i])) );
					}
				}
			}
			if(is_paused)
			al_draw_rectangle(display_width*.98, 2.0*display_width*.01, 
				display_width*.99, display_width*.01, al_map_rgb(255,255,255), 2);
			char str[48];
			sprintf(str,"%f %i ",camera[pitch],(int)mouse_x%360);
			al_draw_text(font, al_map_rgb(255,255,255), display_width/2, display_height/10,ALLEGRO_ALIGN_CENTRE, str);
			if(is_paused)al_draw_filled_circle(
				    mouse_cursor_x,
				    mouse_cursor_y,
				    5.0, al_map_rgb(255,255,255) );
			al_flip_display();
		}
		counter++;
		if (counter >= HOW_MANY_PARTICLES) counter = 0;
	}

	al_destroy_timer(timer);
	al_destroy_display(display);
	al_destroy_event_queue(event_queue);

	/* Free the memory allocated. */
	al_set_sample(sample, NULL);
	al_destroy_sample(sample_data);

	al_destroy_sample_instance(sample);
	al_destroy_mixer(mixer);
	al_destroy_voice(voice);

	al_uninstall_audio();

	return 0;
}
